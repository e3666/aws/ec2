
# EC2
## O que são EC2?
[Documentação](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html)

Uma instância EC2 é nada mais que uma máquina assim como qualquer computador, a diferença é que são máquinas virtualizadas. A vantagem dessas máquinas, além de pagar somente pelo tempo que realmente estiver utilizando, é a facilidade de criar uma, pois a aws já fez a maior parte do trabalho para você, ela já comprou os servidores, montou os data centers, mantém eles seguros, instalou e configurou os sistemas operacionais e já tem todo o sistema de network pronto para que esses servidores fiquem online e acessíveis.

A virtualização funciona com o princípio de `multitenancy`, ou seja, o hypervisor é responsável por compartilhar o hardware da máquina física com os sistemas virtualizados, e também manter o completo isolamento entre eles. E isso é gerenciado pela aws.

## Como o EC2 funciona

Para trabalhar com EC2 você tem três estágios:

- Launch:
    - Primeiro de tudo você cria a instância, para isso você escolhe templates preconfigurados (AMIs) que já contêm informações de sistema operacional, e alguns softwares para a máquina. Você também seleciona o tipo de instância, e especifica informações de hardware, como CPU e memória. Bem como configurações de network e segurança (voltados aos security groups, mas mais disso depois).

- Connect:
    - Com uso de ssh é possível se conectar a máquina utilizando o terminal, ou até mesmo algum outro software, como por exemplo o vscode com a extensão de remote ssh

- Use:
    - Instale sua aplicação, e softwares que serão necessários para ela ficar executando.

## Instance Types
As instâncias são divididas em tipos distintos, cada um desses tipos, que também podem ser chamados de familias, têm um propósito diferente, variando entre instâncias com mais memória, mais cpu, mais armazenamento...

Os tipos são dividos entre:

- `general purpose`:
    - recursos de processamento, memória e armazenamento balanceados.
    - Exemplos de uso:
        - web servers
- `compute optimized`:
    - tarefas que exigem processamento intenso 
    - Exemplos de uso:
        - Processamento em lote
        - Gaming servers
        - Modelagem científica
- `memory optimized`:
    - tarefas que exigem muita memoria
    - Exemplos de uso:
        - Cache
        - Filas
        - Análise de big data em tempo real
- `accelerated computing`:
    - Essas máquinas contam com GPUs
    - Exemplos de uso:
        - Calculo com float
        - Processamento gráfico
        - Machine learning
- `storage optimized`:
    - Quando precisar de máquinas com muito armazenamento e que sejam de acesso rápido ao mesmo tempo
    - Exemplos de uso:
        - Bancos de dados NoSQL
        - Data Warehousing
        - Elasticsearch

Você pode ver todos os tipos de instâncias nesse link [aqui](https://aws.amazon.com/pt/ec2/instance-types/)

## princing
- on-demand
    - você paga pelo tempo que a máquina fica ativa, sendo que o custo/hora dela varia de acordo com o tipo da máquina e sistema operacional.
    - Recomendadas para testes, e utilizações irregulares, ou seja, os processos não tem hora para começar e nem para terminar, mas não podem ser interrompidos. Logo você não sabe ao certo quando ligar ou desligar a máquina
- savings plan
    - O custo é menor (72% mais barato), mas você deve se comprometer a uma utilização consistente do recurso em dolares/hora, por 1 a 3 anos. Ou seja, todo dia a máquina fica ativa por x tempos, o que resolta em y dolares. Qualquer uso até o valor comprometido é cobrado na taxa do plano, porém qualquer quantidade de tempo adicional é cobrada com valor do plano on-demand
- Reserved instances
    - Quando o uso é previsível, e você reserva um tipo de máquina por 1 a 3 anos. Mas se você não usar ela, então paga do mesmo jeito. A economia é de uns 74%.
    - Caso ultrapasse a reserva, você começa a pagar o preço do plano on-demand até que termine a instância, ou crie um novo plano.
- Spot instances
    - Muito mais barato (90%), você pode usar a máquina, a qual utiliza a capidade computacional inutilizada da aws, mas a aws vai poder pegar ela de você a qualquer momento, com apenas dois minutos de aviso. Isso acontece quando a capidade ociosa da cloud começa a não existir mais. Mas assim que as coisas voltam ao normal, a instância volta a operar. Ideal para processos que não tem nenhum problema em serem interrompidos e que executam em backgroud.
- Dedicated Host
    - máquina física somente para você, e você pode reservar ou pagar no plano on-demand, mas de qualquer forma, essa é a opção mais cara de todas.


# Escalabilidade
Você pode ter dois problemas com seus servidores, eles serem pequenos demais para sua demanda, ou grandes demais para ela.

<img src="./img/fig-01.svg"/>

Em um server on-premises você não consegue resolver esse problema, pois a máquina sempre terá a mesma configuração o tempo todo. Mas com a aws você pode escalar a máquina conforme a necessidade. Ou seja, você configura para que a máquina aumente a quantidade de memória e cpu conforme o necessário, e diminua conforme o necessário, assim você paga somente pelo que está precisando, e também não sofre com falta de disponibilidade.

Porém a escalabilidade pode ser tanto aumentando e diminuindo os recursos da máquina, quanto aumentando e diminuindo o número de máquinas. E isso pode variar de caso a caso. Suponha que você tem uma aplicação que consome uma fila, e que processa um elemento da fila por vez. Uma máquina maior faria apenas que cada elemento fosse processado um pouco mais rápido, mas o melhor mesmo mesmo é ter várias aplicações rodando em paralelo para consumirem a fila muito mais rápido ainda. Sendo assim, novas instâncias pequenas são melhores que uma única instância grande. E assim que aliviar os processos, você pode terminar as instâncias, e ficar somente com o minímo.

Ao criar um `auto scaling group` você pode configurar a capacidade mínima, desejada e máxima que seu sistema vai ter. E assim a aws gerencia tudo para você.

# ELB - Elastic load balancing
Com multiplas máquinas, temos múltiplos endpoints, logo nos teremos um problema de rede, pois os clients não sabem onde acessar as máquinas. Para isso colocamos um load balancer na frente. Os clientes vão acessar o load balancer, e ele irá redirecioná-los para uma máquina. Sendo que esse redirecionamento é inteligente, sendo feito para redirecionar o cliente para o servidor que está com a melhor disponibilidade no momento.

Uma vantagem do ELB é que ele pode ser utilizado não só no sistema client/server, mas também na comunicação entre servidores. Se tivermos multiplos serviços fazendo A e outros fazendo B e A precisa conversar com B, então é mais simples que eles se comuniquem por meio do load balancer, assim sempre que uma nova instância de um dos serviços subir, o load balancer cuida de gerenciar as requests.
